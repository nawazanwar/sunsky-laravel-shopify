@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    @include('dashboard.cards')
    <div class="row">
        <div class="col-md-6">
            @include('dashboard.latest_product')
        </div>
        <div class="col-md-6">
            @include('dashboard.latest_variants')
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            @include('dashboard.latest_updated_variants')
        </div>
        <div class="col-md-6">
            @include('dashboard.latest_updated_products')
        </div>
    </div>
@stop
