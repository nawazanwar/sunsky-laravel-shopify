<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('css/vendor.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <title>@yield('pageTitle')</title>

    @stack('styles')

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="login-page">

<div class="overlay opacity-95"></div>
<div class="login-box">
    @yield('content')
</div>
<script src="{{ asset('js/vendor.min.js') }}"></script>
<script src='https://www.google.com/recaptcha/api.js' async defer></script>
</body>
</html>
