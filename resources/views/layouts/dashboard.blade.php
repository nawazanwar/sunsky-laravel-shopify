<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/vendor.min.css') }}" rel="stylesheet">
    @yield('styleInnerFiles')
    <title>@yield('pageTitle')</title>
    @stack('style')
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="sidebar-mini" style="height: auto;">
<div class="wrapper" id="app">
    {{-- Header--}}
    @include('partials.dashboard.header')
    {{-- left_side--}}
    @include('partials.dashboard.sidebar')
    {{-- Main Content--}}
    <div class="content-wrapper" style="min-height: 470px;">
        @yield('breadcrumbs')
        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
        {{-- Footer--}}
        {{--@include('partials.dashboard.footer')--}}
    </div>
    <script src="{{ asset('js/vendor.min.js') }}"></script>
@yield('scriptInnerFiles')
@yield('pageScript')
</body>
</html>
