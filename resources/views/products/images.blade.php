@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <div class="card card-solid">
        @can('read',\App\Models\Product::class)
            <div class="card-header">
                <div class="card-title">
                    <a class="btn bg-gradient-info"
                       href="{{ route('catalog.products',['pId'=>$model->id]) }}">
                        <i class="fa fa-arrow-circle-left"></i> Back to Product</a>
                    <a class="btn bg-gradient-success text-white pull-right"
                       href="{{ route('catalog.product.images.download',[$model->itemNo]) }}">
                        Download Images
                    </a>
                </div>
            </div>
        @endcan
        <div class="card-body pb-0">
            {{-- all variant images--}}
            <div class="card">
                <div class="card-header">
                    <div class="card-title font-weight-bold">
                        All Variant Images
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        @if(count($model->variantImages)>0)
                            @foreach($model->variantImages as $key=>$variantImage)
                                <div class="col-md-3 my-2 text-center">
                                    <img class="img-thumbnail img-fluid" src="{{ $variantImage }}" alt="{{ $key }}">
                                    <p class="py-2 font-weight-bold">{{ $key }}</p>
                                </div>
                            @endforeach
                        @else
                            <p class="text-center">No Variant Image</p>
                        @endif
                    </div>
                </div>
            </div>
            {{-- all Gallery Images--}}
            <div class="card">
                <div class="card-header">
                    <div class="card-title font-weight-bold">
                        All Gallery images of <a
                            href="{{ route('catalog.products',['pId'=>$model->id]) }}">{{ $model->itemNo }}</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach($model->images as $image)
                            <div class="col-md-3 my-2">
                                <img src="{{ $image }}" class="img img-fluid img-thumbnail">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
