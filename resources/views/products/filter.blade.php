<form action="{{ route('catalog.products') }}" method="get">
    <div class="row justify-content-center">
        <div
            class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 col-12 form-group form-group-sm mb-xl-0 mb-lg-0 mb-xl-0">
            <select class="custom-select custom-select-sm" name="field">
                <option value="title" @if(isset($field) and $field == 'title') selected @endif>
                    Title
                </option>
                <option value="itemNo"
                        @if(isset($field) and $field == 'itemNo') selected @endif>
                    Item No
                </option>
            </select>
        </div>
        <div
            class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 col-12 form-group form-group-sm mb-xl-0 mb-lg-0 mb-xl-0">
            <input type="search" name="keyword"
                   value="@if(isset($keyword) and $keyword != '') {{ $keyword }} @endif"
                   class="form-control form-control-sm"
                   placeholder="{{__('system.search_here')}}">
        </div>
        <div
            class="col-xl-1 col-lg-1 col-md-1 col-sm-12 col-xs-12 col-12 form-group form-group-sm mb-xl-0 mb-lg-0 mb-xl-0">
            <input type="submit" class="btn btn-primary btn-sm" value="Filter">
        </div>
    </div>
</form>
