@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection


@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header">
                    @can('read',\App\Models\Product::class)
                        <div class="row align-items-center">
                            <div class="col-md-8 text-left">
                                <button type="submit" class="btn btn-xs bg-gradient-info text-white">Sync Selected to
                                    Shopify
                                </button>
                            </div>
                            <div class="col-md-4">
                                <form class="text-center" method="get" action="{{ route('catalog.products.search') }}">
                                    <div class="input-group input-group-sm">
                                        <input class="form-control form-control-navbar" type="search" name="itemNo"
                                               placeholder="enter product code"
                                               aria-label="Search">
                                        <div class="input-group-append">
                                            <button class="btn bg-gradient-success btn-sm">Generate</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    @endcan
                </div>
                <div class="card-header" style="background-color: #f4f6f9">
                    @include('products.filter')
                </div>
                <form method="post" action="{{route('catalog.products.sync.selected')}}">
                    {{ csrf_field() }}
                    <div class="card-body">
                        @include('partials.dashboard.message')
                        <div class="card">
                            <style>
                                .table td, .table th {
                                    vertical-align: middle;
                                }
                            </style>
                            <div class="card-header">
                                <h3 class="card-title font-weight-bold">List of all Products</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table class="table table-bordered table-striped table-hover">
                                    <tr>
                                        <th>
                                            <div class="icheck-success d-inline">
                                                <input type="checkbox" id="checkAll">
                                                <label for="checkAll">
                                                </label>
                                            </div>
                                        </th>
                                        <th>Item No #</th>
                                        <th class="text-center">Total Variants</th>
                                        <th class="text-center">Synced</th>
                                        <th class="text-center">Is Updated</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    @if(count($products)>0)
                                        @foreach($products as $key=>$p)
                                            <tr class="align-items-center">
                                                <td style="width: 10px;">
                                                    <div class="icheck-primary d-inline">
                                                        <input name='id[]' type="checkbox" id="checkItem-{{$p->id}}"
                                                               value="<?php echo $products[$key]->id; ?>">
                                                        <label for="checkItem-{{$p->id}}"></label>
                                                    </div>
                                                </td>
                                                <td>
                                                    {{ $p->itemNo }}
                                                </td>
                                                <td class="text-center">{{ $p->variants->count() }}</td>
                                                <td class="text-center">
                                                    @if($p->is_created)
                                                        <i class="fa fa-check text-success"></i>
                                                    @else
                                                        <i class="fa fa-times text-danger"></i>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if($p->is_updated)
                                                        <i class="fa fa-check text-success"></i>
                                                    @else
                                                        <i class="fa fa-times text-danger"></i>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @can('read',\App\Models\Product::class)
                                                        <a class="btn bg-gradient-success btn-xs text-white"
                                                           href="{{ route('catalog.product.images',[$p->itemNo]) }}">
                                                            images
                                                        </a>
                                                    @endcan
                                                    @can('read',\App\Models\Variant::class)
                                                        <a class="btn btn-info btn-xs text-white"
                                                           href="{{ route('catalog.variants',['pId'=>$p->id]) }}">
                                                            Variants <i class="fa fa-eye "></i>
                                                        </a>
                                                    @endcan
                                                    @can('edit',\App\Models\Product::class)
                                                        <a href="{{ route('catalog.products.edit',[$p->id]) }}"
                                                           class="btn bg-gradient-dark btn-xs">
                                                            <i class="fas fa-edit"></i>
                                                        </a>
                                                    @endcan
                                                    @can('sync',\App\Models\Product::class)
                                                        <a href="{{ route('products.sync',[$p->id]) }}"
                                                           class="btn bg-gradient-success btn-xs">
                                                            Sync Shopify <i class="fa fa-arrow-alt-circle-right"></i>
                                                        </a>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                        @if( method_exists($products,'links') )
                                            <tr>
                                                <td colspan="13" class="text-center">
                                                    {{ $products->links() }}
                                                </td>
                                            </tr>
                                        @endif
                                    @else
                                        <tr>
                                            <td colspan="7" class="text-center"> No Product Found</td>
                                        </tr>
                                    @endif
                                </table>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
        @stop
        @section('scriptInnerFiles')
            <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
            <script language="javascript">
                $("#checkAll").click(function () {
                    $('input:checkbox').not(this).prop('checked', this.checked);
                });
            </script>
@endsection
