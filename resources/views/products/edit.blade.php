@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <div class="card card-solid">
        @can('read',\App\Models\Product::class)
            <div class="card-header">
                <div class="card-title">
                    {!! link_to_route('catalog.products',__('system.all_products'),null,['class'=>'btn btn-info btn-sm']) !!}
                </div>
            </div>
        @endcan
        <div class="card-body pb-0">
            <!-- form start -->
            {!! Form::model($model, ['route' => ['catalog.products.update', $model->id], 'method' => 'PUT'] ) !!}
            {!! csrf_field() !!}
            @include('partials.dashboard.message')
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('itemNo',"Item Number") !!}
                        {!! Form::text('itemNo', null, ['class' => 'form-control', 'id' => 'itemNo',"disabled"=>"disabled"]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('price',"Price") !!}
                        {!! Form::text('price', null, ['class' => 'form-control', 'id' => 'price']) !!}
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        {!! Form::label('title',"Title") !!}
                        {!! Form::text('title', null, ['class' => 'form-control', 'id' => 'title']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('vendor',"Vendor") !!}
                        {!! Form::text('vendor', null, ['class' => 'form-control', 'id' => 'vendor']) !!}
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        {!! Form::label('body_html',"Body Html") !!}
                        {!! Form::textarea('body_html', null, ['class'=>'form-control','id' => 'body_html', 'rows' => 5]) !!}
                    </div>
                </div>
            </div>
            @if($model->variants->count()>0)
                <div class="row">
                    <div class="col-12">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">List of all Variants</h3>
                            </div>
                            <div class="card-body">
                                @foreach($model->variants as $vkey=>$variant)
                                    <input type="hidden" name="variants[id][]" value="{{ $variant->id }}">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="sku-{{ $vkey }}">SKU</label>
                                            <input name="variants[sku][]" class="form-control" id="sku-{{ $vkey }}" value="{{ $variant->sku }}" readonly>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="title-{{ $vkey }}">Color</label>
                                            <input name="variants[title][]" class="form-control" id="title-{{ $vkey }}" value="{{ $variant->title }}">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="price-{{ $vkey }}">Price</label>
                                            <input name="variants[price][]" class="form-control" id="price-{{ $vkey }}" value="{{ $variant->price }}">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="inventory_quantity-{{ $vkey }}">Quantity</label>
                                            <input name="variants[inventory_quantity][]" class="form-control" id="inventory_quantity-{{ $vkey }}" value="{{ $variant->inventory_quantity }}">
                                        </div>
                                        {{--<div class="form-group col-md-2">
                                            <label for="barcode-{{ $vkey }}">Bar Code</label>
                                            <input name="variants[barcode][]" class="form-control" id="barcode-{{ $vkey }}" value="{{ $variant->barcode }}">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="grams-{{ $vkey }}">Grams</label>
                                            <input name="variants[grams][]" class="form-control" id="grams-{{ $vkey }}" value="{{ $variant->grams }}">
                                        </div>--}}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="form-group text-right">
                {!! Form::submit(__('system.update'), array('class' => 'btn btn-primary pull-left btn-sm')) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script
        src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=o23blg9ah1zca8fzz4hpnqndoxanbov1pv288bvw0ptznp19"></script>
    <script type="text/javascript">
        $(function () {
            // Replace the textarea with tinyMCE
            tinymce.init({
                selector: '#body_html',
                height: 200,
                theme: 'modern',
                directionality: 'ltr',
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern image imagetools"
                ],
                toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                toolbar2: "print preview media | forecolor backcolor emoticons",
                image_advtab: true,
                relative_urls: false,
                remove_script_host: false,
                images_upload_url: '/muntazim/posts/uploader',
                images_upload_base_path: '/uploads/posts',
                image_caption: true,
                file_picker_callback: function (callback, value, meta) {
                    if (meta.filetype == 'image') {
                        $('#upload').trigger('click');
                        $('#upload').on('change', function () {
                            var file = this.files[0];
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                callback(e.target.result, {
                                    alt: ''
                                });
                            };
                            reader.readAsDataURL(file);
                        });
                    }
                },
                templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
                ],
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tinymce.com/css/codepen.min.css'
                ]
            });
        });
    </script>
@endsection
