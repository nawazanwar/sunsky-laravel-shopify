@extends('layouts.auth')
@section('pageTitle', $pageTitle)
@section('content')
    <div class="card">
        <div class="card-body login-card-body">
            <h5 class="login-box-msg">{{__('auth.login_heading')}}</h5>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="input-group mb-3">
                    <input id="email" type="email" class="form-control b-n @error('email') is-invalid @enderror"
                           name="email" value="{{ old('email') }}" required autocomplete="off"
                           autofocus
                           placeholder="{{ __('auth.enter_email') }}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope text-white"></span>
                        </div>
                    </div>
                    @error('email')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>
                <div class="input-group mb-3">
                    <input id="password" type="password" class="form-control b-n  @error('password') is-invalid @enderror"
                          onautocomplete="off" name="password" required autocomplete="current-password" placeholder="{{ __('auth.enter_password')}}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock text-white"></span>
                        </div>
                    </div>
                    @error('password')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary btn-sm btn-dark">{{__('auth.login')}}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
