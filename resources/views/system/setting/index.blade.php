@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-body">
                    @include('partials.dashboard.message')
                    {!! Form::open(['route' => 'system.settings.update', 'files' => true] ) !!}
                    {!! csrf_field() !!}
                    <div class="card">
                        <div class="card-body">
                            @foreach($settings as $setting)
                                @if($setting->type == 'text')
                                    <div class="form-group">
                                        <label for="settingValue{{$setting->id}}">{{ str_replace('_', ' ', $setting->name)}}</label>
                                        <input type="text" name="{{$setting->name}}" class="form-control"
                                               id="settingValue{{$setting->id}}" value="{{$setting->value}}">
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group text-center">
                        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
