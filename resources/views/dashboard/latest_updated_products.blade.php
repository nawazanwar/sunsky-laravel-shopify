<div class="card card-primary">
    <div class="card-header border-transparent">
        <h3 class="card-title">Latest Updated Products</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0" style="display: block;">
        <div class="table-responsive">
            <table class="table m-0">
                @php
                    $updated_products =  \App\Models\Product::where('is_updated',true)->orderBy('updated_at', 'desc')->limit(5)->get()
                @endphp
                @if(count($updated_products)>0)
                    @foreach($updated_products as $p)
                        <tr class="align-items-center">
                            <td>{{ $p->itemNo }}</td>
                            <td class="text-center">
                                @can('read',\App\Models\Variant::class)
                                    <a class="btn btn-info btn-xs text-white"
                                       href="{{ route('catalog.variants',['pId'=>$p->id]) }}">
                                        Variants <i class="fa fa-eye "></i>
                                    </a>
                                @endcan
                                @can('edit',\App\Models\Product::class)
                                    <a href="{{ route('catalog.products.edit',[$p->id]) }}"
                                       class="btn bg-gradient-dark btn-xs">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                @endcan
                                @can('sync',\App\Models\Product::class)
                                    <a href="{{ route('products.sync',[$p->id]) }}"
                                       class="btn bg-gradient-success btn-xs">
                                        Sync Shopify <i class="fa fa-arrow-alt-circle-right"></i>
                                    </a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7" class="text-center"> No Product Found</td>
                    </tr>
                @endif
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
</div>
