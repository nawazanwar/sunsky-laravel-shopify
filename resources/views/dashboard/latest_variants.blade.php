<div class="card card-success">
    <div class="card-header border-transparent">
        <h3 class="card-title">Latest Created Variants</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0" style="display: block;">
        <div class="table-responsive">
            @php
                $created_variants =  \App\Models\Variant::orderBy('created_at', 'desc')->limit(5)->get()
            @endphp
            <table class="table m-0">
                @if(count($created_variants)>0)
                    @foreach($created_variants as $v)
                        <tr>
                            <td>
                                <a href="{{ route('catalog.products',["pId"=>$v->product->id]) }}">{{ $v->product->itemNo }}</a>
                            </td>
                            <td>{{ $v->title }}</td>
                            <td class="text-center">
                                @can('edit',\App\Models\Variant::class)
                                    <a href="{{ route('catalog.variants.edit',[$v->id]) }}"
                                       class="btn bg-gradient-info btn-xs">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7" class="text-center"> No Variant Found</td>
                    </tr>
                @endif
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
</div>
