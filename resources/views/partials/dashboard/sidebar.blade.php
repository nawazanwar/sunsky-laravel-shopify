@php
    $cRouteName = \Request::route()->getName();
@endphp
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('home') }}" class="brand-link text-center" style="background-color: white;">
        <img src="https://img.sunsky-online.com/htdocs/images/v2_logo.png" style="width:148px;">
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-link active py-0 my-2">
                    <a href="{{ route('home') }}" class="nav-link active">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item has-treeview @if(strpos(Request::url(), 'catalog')) {{ 'menu-open' }} @endif">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-lock"></i>
                        <p>
                            Catalog
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @can('read',\App\Models\Product::class)
                            <li class="nav-item ">
                                <a class="nav-link {{ ($cRouteName=='catalog.products'?'active':'') }}"
                                   href="{{route('catalog.products')}}">
                                    <i class="far fa-circle nav-icon {{ ($cRouteName=='catalog.products'?'text-primary':'') }}"></i>
                                    All Products
                                </a>
                            </li>
                        @endcan
                        @can('read',\App\Models\Variant::class)
                            <li class="nav-item ">
                                <a class="nav-link {{ ($cRouteName=='catalog.variants'?'active':'') }}"
                                   href="{{route('catalog.variants')}}">
                                    <i class="far fa-circle nav-icon {{ ($cRouteName=='catalog.variants'?'text-primary':'') }}"></i>
                                    All Variants
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
                <li class="nav-item has-treeview @if(strpos(Request::url(), 'system')) {{ 'menu-open' }} @endif">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-lock"></i>
                        <p>
                            System
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @can('read',\App\Models\Role::class)
                            <li class="nav-item ">
                                <a class="nav-link {{ ($cRouteName=='system.roles'?'active':'') }}"
                                   href="{{route('system.roles')}}">
                                    <i class="far fa-circle nav-icon {{ ($cRouteName=='system.roles'?'text-primary':'') }}"></i>
                                    All Roles
                                </a>
                            </li>
                        @endcan
                        @can('read',\App\Models\Permission::class)
                            <li class="nav-item ">
                                <a class="nav-link {{ ($cRouteName=='system.permissions'?'active':'') }}"
                                   href="{{route('system.permissions')}}">
                                    <i class="far fa-circle nav-icon {{ ($cRouteName=='system.permissions'?'text-primary':'') }}"></i>
                                    All Permissions
                                </a>
                            </li>
                        @endcan
                        @can('sync',\App\Models\Permission::class)
                            <li class="nav-item">
                                <a class="nav-link {{ ($cRouteName=='system.permissions.sync'?'active':'') }}"
                                   href="{{route('system.permissions.sync')}}">
                                    <i class="far fa-circle nav-icon {{ ($cRouteName=='system.permissions.sync'?'text-primary':'') }}"></i>
                                    Sync Permissions
                                </a>
                            </li>
                        @endcan
                        @can('read',\App\Models\User::class)
                            <li class="nav-item ">
                                <a class="nav-link {{ ($cRouteName=='system.users'?'active':'') }}"
                                   href="{{route('system.users')}}">
                                    <i class="far fa-circle nav-icon {{ ($cRouteName=='system.users'?'text-primary':'') }}"></i>
                                    All Users
                                </a>
                            </li>
                        @endcan
                        @can('read',\App\Models\Setting::class)
                            <li class="nav-item ">
                                <a class="nav-link {{ ($cRouteName=='system.settings'?'active':'') }}"
                                   href="{{route('system.settings')}}">
                                    <i class="far fa-circle nav-icon {{ ($cRouteName=='system.settings'?'text-primary':'') }}"></i>
                                    Settings
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
