<form action="{{ route('catalog.variants') }}" method="get">
    <div class="row justify-content-center">
        <div
            class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 col-12 form-group form-group-sm mb-xl-0 mb-lg-0 mb-xl-0">
            <select class="custom-select custom-select-sm" name="field">
                <option value="sku" @if(isset($field) and $field == 'sku') selected @endif>
                    Sku
                </option>
                <option value="title" @if(isset($field) and $field == 'title') selected @endif>
                    Color
                </option>
                <option value="price" @if(isset($field) and $field == 'price') selected @endif>
                    Price
                </option>
            </select>
        </div>
        <div
            class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 col-12 form-group form-group-sm mb-xl-0 mb-lg-0 mb-xl-0">
            <input type="search" name="keyword"
                   value="@if(isset($keyword) and $keyword != '') {{ $keyword }} @endif"
                   class="form-control form-control-sm"
                   placeholder="{{__('system.search_here')}}">
        </div>
        <div
            class="col-xl-1 col-lg-1 col-md-1 col-sm-12 col-xs-12 col-12 form-group form-group-sm mb-xl-0 mb-lg-0 mb-xl-0">
            <input type="submit" class="btn btn-primary btn-sm" value="Filter">
        </div>
    </div>
</form>
