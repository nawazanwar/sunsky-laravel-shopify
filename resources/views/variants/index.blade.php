@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-body">
                    @include('partials.dashboard.message')
                    <div class="card">
                        <style>
                            .table td, .table th {
                                vertical-align: middle;
                            }
                        </style>
                        <div class="card-header">
                            <h3 class="card-title font-weight-bold">List of all Variants</h3>
                        </div>
                        <div class="card-header" style="background-color: #f4f6f9">
                            @include('variants.filter')
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-bordered table-striped table-hover">
                                <tr>
                                    <th>Product</th>
                                    <th>Sku</th>
                                    <th>Color</th>
                                    <th>Price</th>
                                    {{--<th class="text-center">Bar Code</th>--}}
                                    <th class="text-center">Inventory</th>
                                    {{-- <th class="text-center">Grams</th>--}}
                                    <th class="text-center">Is Updated</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                @if(count($variants)>0)
                                    @foreach($variants as $v)
                                        <tr class="align-items-center">
                                            <td>
                                                <a href="{{ route('catalog.products',["pId"=>$v->product->id]) }}">{{ $v->product->itemNo }}</a>
                                            </td>
                                            <td>{{ $v->sku }}</td>
                                            <td>{{ $v->title }}</td>
                                            <td>
                                                {{ $v->price }}
                                            </td>
                                            {{--<td>
                                                {{ $v->barcode }}
                                            </td>--}}
                                            <td class="text-center">
                                                {{ $v->inventory_quantity }}
                                            </td>
                                            {{-- <td>
                                                 {{ $v->grams }}
                                             </td>--}}
                                            <td class="text-center">
                                                @if($v->is_updated)
                                                    <i class="fa fa-check text-success"></i>
                                                @else
                                                    <i class="fa fa-times text-danger"></i>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @can('edit',\App\Models\Variant::class)
                                                    <a href="{{ route('catalog.variants.edit',[$v->id]) }}"
                                                       class="btn bg-gradient-info btn-xs">
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                    @if( method_exists($variants,'links') )
                                        <tr>
                                            <td colspan="13" class="text-center">
                                                {{ $variants->links() }}
                                            </td>
                                        </tr>
                                    @endif
                                @else
                                    <tr>
                                        <td colspan="7" class="text-center"> No Variants Found</td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
