@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <div class="card card-solid">
        @can('read',\App\Models\Variant::class)
            <div class="card-header">
                <div class="card-title">
                    {!! link_to_route('catalog.variants',__('system.all_variants'),null,['class'=>'btn btn-info btn-sm']) !!}
                </div>
            </div>
        @endcan
        <div class="card-body pb-0">
            <!-- form start -->
            {!! Form::model($model, ['route' => ['catalog.variants.update', $model->id], 'method' => 'PUT'] ) !!}
            {!! csrf_field() !!}
            @include('partials.dashboard.message')
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('sku',"SKU") !!}
                        {!! Form::text('sku', null, ['class' => 'form-control', 'id' => 'sku',"disabled"=>"disabled"]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('price',"Price") !!}
                        {!! Form::text('price', null, ['class' => 'form-control', 'id' => 'price']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('barcode',"Bar Code") !!}
                        {!! Form::text('barcode', null, ['class' => 'form-control', 'id' => 'barcode']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('title',"Title") !!}
                        {!! Form::text('title', null, ['class' => 'form-control', 'id' => 'title']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('inventory_quantity',"Inventory Quantity") !!}
                        {!! Form::text('inventory_quantity', null, ['class' => 'form-control', 'id' => 'inventory_quantity']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('grams',"Grams") !!}
                        {!! Form::text('grams', null, ['class' => 'form-control', 'id' => 'grams']) !!}
                    </div>
                </div>
            </div>
            <div class="form-group text-right">
                {!! Form::submit(__('system.update'), array('class' => 'btn btn-primary pull-left btn-sm')) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
