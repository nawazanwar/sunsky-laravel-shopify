<?php

return [
    'manage'=>'Manage',
    'all_images'=>'List of all images of ',
    'all_variants' =>'All Variants',
    'all_products'=>'All Products',
    'all_permissions' => 'All Permissions',
    'all_roles' => 'All Roles',
    'show' => 'Show',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'permissions' => 'Permissions',
    'create' => 'Create',
    'search' => 'Search',
    'search_here' => 'Please search here....',
    'save' => 'Save',
    'name' => 'Name',
    'label' => 'Label',
    'placeholder_name' => 'Enter Name..',
    'placeholder_label' => 'Enter Label..',
    'create_permission' => 'Create Permission',
    'create_role' => 'Create Role',
    /* Roles */
    'role_created_success_message' => 'Role Created SuccessFully',
    'role_updated_success_message' => 'Role Updated Successfully',
    'role_created_failed_message_message' => 'Role Created Failed',
    'role_updated_failed_message_message' => 'Role Updated Failed',
    /* Permissions */
    'permission_created_success_message' => 'Permission Created SuccessFully',
    'updated_success_message' => 'Updated Successfully',
    'permission_created_failed_message' => 'Permission Created Failed',
    'updated_failed_message' => 'Updated Failed',
    'edit_role' => 'Edit Role',
    'edit_permission' => 'Edit Permission',
    'update' => 'Update',
    'deleted_successfully' => 'Deleted Successfully',
    'alert' => '!Alert',
    'all_permissions_of' => 'All permissions of',
    'permission_sync' => 'Permissions synced!',
    'failed_permission_sync' => 'Failed to sync permissions!',
    'all_users' => 'All Users',
    'create_user' => 'Create User',
    'email' => 'Email',
    'placeholder_email' => 'test@domain.com',
    'temp_pwd' => 'Temporary Password',
    'placeholder_temp_password' => 'Enter Temporary password....',
    'is_active' => 'Is Active',
    'all_roles_of' => 'All Roles of',
    'all_users_of' => 'All Users of',
    'date_of_birth' => 'Date of Birth',
    'placeholder_date_of_birth' => 'Enter date of birth',
    'settings' => 'Settings'
];
