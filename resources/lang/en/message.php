<?php

return [
    'no_permission' => 'Sorry you have no permission to perform this action',
    'user_created_success_message' => 'New User has been created Successfully',
    'user_created_failed_message' => 'Failed to create a new user',
    'no_user_found' => "No User Found...",
    "no_role_found" => "No Role Found...",
    "successfully_deactivated" => 'Successfully De activated',
    "successfully_activated" => 'Successfully Activated',
    'assigned_successfully' => 'Assigned Successfully',
    'removed_successfully' => 'Removed Successfully',
    'no_people_found' => 'No People Found...',
    'no_vehicle_found' => 'No Vehicle Found...',
    'house_created_success_message' => 'House created successfully',
    'user_sync_successfully' => 'User sync Successfully',
    'maintenance_mode' => 'System is in maintenance Mode.',
    'setting_updated_successfully' => 'Setting Updated Successfully'
];
