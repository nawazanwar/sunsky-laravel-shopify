<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        User::create([
            'name' => 'Super Admin',
            'email' => 'super@gmail.com',
            'password' =>Hash::make('super_1234'),
            'active' => 1,
        ]);
        User::create([
            'name' => 'User',
            'email' => 'user@gmail.com',
            'password' =>Hash::make('user_1234'),
            'active' => 1,
        ]);
    }
}
