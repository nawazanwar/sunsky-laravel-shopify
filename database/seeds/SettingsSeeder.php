<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Setting;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->delete();
        Setting::create([
            'name' => 'sun_key',
            'value' => "sunskyapitest2",
            'type' => 'text',
        ]);
        Setting::create([
            'name' => 'sun_secret',
            'value' => 'xd82clwq2pa3',
            'type' => 'text',
        ]);
        Setting::create([
            'name' => 'sun_url',
            'value' => 'http://www.sunsky-api.com/openapi/',
            'type' => 'text',
        ]);
        Setting::create([
            'name' => 'shopify',
            'value' => 'https://09e28c1937cffd01f910a1b8e519c24e:c172ec4947de1e11e80ce02bf42cb790@sunsky-integration.myshopify.com/admin/api/2020-01/',
            'type' => 'text',
        ]);
    }
}
