<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        Role::create([
            'name' => 'super-admin',
            'label' => 'Super Admin'
        ]);

        Role::create([
            'name' => 'user',
            'label' => 'Admin'
        ]);
    }
}
