<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Role;
class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_user')->delete();
        DB::table('role_user')->insert([
            'role_id' => Role::where('name','super-admin')->value('id'),
            'user_id' => User::where('email','super@gmail.com')->value('id'),
        ]);
    }
}
