<?php

namespace App\Http\Controllers;

use App\Models\Variant;
use Illuminate\Http\Request;

class VariantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        $pageTitle = "Variants";
        $variants = new Variant();
        $pId = $request->query('pId', null);
        if ($request->has('pId') && $pId != '') {
            $variants = $variants->where('product_id', $pId);
        }
        if ($field && $keyword) {
            $variants = $variants->where('variants.' . $field, 'like', '%' . $keyword . '%');
        }
        $variants = $variants->paginate(50);
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'variants' => $variants,
            'field' => $field,
            'keyword' => $keyword
        ];
        return view('variants.index', $viewParams);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    // Edit product

    public function edit(Request $request, $id)
    {
        $model = Variant::find($id);
        $pageTitle = __('system.edit') . " " . $model->itemNo;
        $breadcrumbs = [['text' => __('system.edit')]];
        $viewParams = [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle
        ];
        return view('variants.edit', $viewParams);
    }

    // Update Product

    public function update(Request $request, $id)
    {
        $model = Variant::find($id);
        $model->price = $request->input('price', null);
        $model->barcode = $request->input('barcode', null);
        $model->title = $request->input('title', null);
        $model->grams = $request->input('grams', null);
        $model->inventory_quantity = $request->input('inventory_quantity', null);
        $model->is_updated = true;
        if ($model->save()) {
            return redirect()->route('catalog.variants')->with('successMessage', $model->sku . " " . __('system.updated_success_message'));
        } else {
            return redirect()->back()
                ->withErrors($model->sku . " " . __('system.updated_failed_message_message'))
                ->withInput();
        }
    }

}
