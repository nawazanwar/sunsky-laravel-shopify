<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::all();
        $pageTitle = __('system.settings');
        $breadcrumbs = [['text' =>$pageTitle]];
        $viewParams = [
            'settings' => $settings,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
        ];
        return view('system.setting.index', $viewParams);
    }

    public function update(Request $request)
    {
        $input = $request->all();
        $settings = Setting::where('type', 'text')->get();

        foreach($settings as $setting) {
            $input[$setting->name] = $request->input($setting->name, 0);
        }

        foreach ($input as $key => $value) {

            if (!strpos($key, '_token') && $key) {

                Setting::where('name', $key)
                    ->update(['value' => $value]);

            }
        }
        return redirect()->route('system.settings');
    }
}
