<?php

namespace App\Http\Controllers\System;

use App\Models\Media;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = new User();
        $data = $data->with('media');
        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        if ($field && $keyword) {
            $data = $data->where('users.' . $field, 'like', '%' . $keyword . '%');
        }
        $data = $data->orderby('users.id', 'DESC')->paginate(10);
        $pageTitle = __('system.all_users');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'field' => $field,
            'keyword' => $keyword,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => $data,
        ];
        return view('system.users.index', $viewParams);
    }

    public function create()
    {

        $pageTitle = __('system.create_user');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
        ];

        return view('system.users.create', $viewParams);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|max:255|unique:users,email',
            'name' => 'required|max:255',
            'password' => 'required|max:255|between:6,20',
            'active' => 'bool|required',
            'image' => 'mimes:jpeg,jpg,png,gif|nullable|max:5000',
            'cover' => 'mimes:jpeg,jpg,png,gif|nullable|max:5000',
        ]);
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->active = $request->input('active', 1);
        $user->password = Hash::make($request->input('password'));
        if ($user->save()) {
            $userImageFolder = public_path('uploads/users');
            if ($request->file('image')) {
                $imageName = sha1('img' . $user->id . time()) . '.' . $request->file('image')->getClientOriginalExtension();
                $img = Image::make($request->file('image'));
                $img->save($userImageFolder . '/' . $imageName);
                $user->image = $imageName;
                $user->save();
                $img->destroy();
                $mediaData = [];
                $mediaData[] = new Media([
                    'title' => $user->title,
                    'path' => $user->image,
                    'type' => 'image',
                    'size' => 'full',
                    'url' => null,
                ]);
                $newImagePath = public_path($user->image);
                $image = Image::make($newImagePath);
                $image->backup();
                foreach (config('image.size') as $key => $size) {
                    $image->reset();
                    $image->resize($size, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $image->save(public_path('uploads/users/' . $image->filename . '_' . $size . '.' . $image->extension));
                    $mediaData[] = new Media([
                        'title' => $user->name,
                        'path' => $image->filename . '.' . $image->extension,
                        'type' => 'image',
                        'size' => $key,
                        'url' => null,
                    ]);
                    $image->backup();
                }
                $image->destroy();
                $user->media()->saveMany($mediaData);
            }
            return redirect()->route('system.users')->with('successMessage', __('system.user_created_success_message'));
        } else {
            return redirect()->back()->withErrors(__('system.user_created_failed_message'))->withInput();
        }
    }

    public function edit($id)
    {
        $model = User::find($id);
        $pageTitle = __('system.edit') . " " . $model->label;
        $breadcrumbs = [['text' => __('system.edit_user')]];
        $viewParams = [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle
        ];

        return view('system.users.edit', $viewParams);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|min:4|unique:roles,name,' . $id,
            'label' => 'required|max:255',
        ]);

        if (Role::whereId($id)->update($validatedData)) {
            return redirect()->route('system.roles')->with('successMessage', $validatedData['label'] . " " . __('system.updated_success_message'));
        } else {
            return redirect()->back()
                ->withErrors($validatedData['label'] . " " . __('system.updated_failed_message_message'))
                ->withInput();
        }
    }

    public function destroy($id)
    {
        $model = User::find($id);
        if ($model->delete()) {
            return redirect()->route('system.users')->with('errorMessage', $model->name . " " . __('system.deleted_successfully'));
        }
    }

    public function roles($id, Request $request)
    {
        $model = User::find($id);
        $roles = Role::all();
        $pageTitle = __('system.all_roles_of') . " " . $model->name;
        $breadcrumbs = [['text' => __('system.all_permissions')]];
        $viewParams = [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'roles' => $roles
        ];
        return view('system.users.roles', $viewParams);
    }

    public function assignRole($uId, $rId, $type)
    {
        $model = User::find($uId);
        $role = Role::find($rId);
        if ($type == 'deActive') {
            $model->roles()->detach($rId);
        } else {
            $model->roles()->save($role);
        }
        return response()->json(['type' => $type, 'label' => $role->label]);
    }

    public function optimize($id, $asFunction = false, $edit = false)
    {
        $imageSizes = config('image.size');
        $user = User::find($id);
        if ($user && isset($user->image) && $user->image) {
            $imagePath = public_path('uploads/users/' . $user->image);
            $mediaData = [];
            $mediaData[] = new Media([
                'title' => $user->name,
                'path' => $user->image,
                'type' => 'image',
                'size' => 'full',
                'url' => null,
            ]);
            $image = Image::make($imagePath);
            $image->backup();
            $imageFileName = $image->filename;
            $imageExtension = $image->extension;
            foreach ($imageSizes as $key => $size) {
                $image->reset();
                $image->resize($size, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $image->save(public_path('uploads/users/' . $imageFileName . '_' . $size . '.' . $imageExtension));
                $mediaData[] = new Media([
                    'title' => $user->name,
                    'path' => $image->filename . '.' . $image->extension,
                    'type' => 'image',
                    'size' => $key,
                    'url' => null,
                ]);
                $image->backup();
            }
            $image->destroy();
            if ($edit) {
                $user->media()->delete();
            }
            $user->media()->saveMany($mediaData);
            if (!$asFunction) {
                return redirect()->back();
            }
            return true;
        }
        return redirect()->back()
            ->withErrors('Invalid User or no images found for this post!')
            ->withInput();
    }
}
