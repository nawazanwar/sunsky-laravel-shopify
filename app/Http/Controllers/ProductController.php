<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Setting;
use App\Models\Shopify;
use App\Models\SunKey;
use App\Models\Variant;
use App\Traits\General;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use ZanySoft\Zip\Zip;

class ProductController extends Controller
{
    use General;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        $pageTitle = __('dashboard.products');
        $products = new Product();
        $pId = $request->query('pId', null);
        if ($request->has('pId') && $pId != '') {
            $products = $products->where('id', $pId);
        }
        if ($field && $keyword) {
            $products = $products->where('products.' . $field, 'like', '%' . $keyword . '%');
        }
        $products = $products->orderBy('id', 'desc')->paginate(50);
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'products' => $products,
            'field' => $field,
            'keyword' => $keyword
        ];
        return view('products.index', $viewParams);
    }

    public function search(Request $request)
    {

        $sunKey = new SunKey();
        $itemNo = $request->get('itemNo', null);
        $route = 'product!detail.do';
        $response = $sunKey->call($route, array('itemNo' => $itemNo));

        if ($response->result == 'success') {

            $product = $response->data;
            $product_description = isset($product->description) ? $product->description : "";
            $product_paramsTable = isset($product->paramsTable) ? $product->paramsTable : "";
            $product_description = $product_description . $product_paramsTable;

            $modal = Product::updateOrCreate(['itemNo' => $product->itemNo],
                [
                    'title' => isset($product->name) ? $product->name : '',
                    'body_html' => $product_description,
                    'price' => isset($product->price) ? $product->price : 0
                ]
            );

            /* Varient Management */

            if (isset($product->modelList)) {

                $this->addVarient($product->modelList, $itemNo, $sunKey, $route, $modal);

            }

            /* Inner management */

            if (isset($product->optionList)) {

                $this->createInnerProduct($product->optionList, $itemNo, $sunKey, $route);

            }

            return redirect()->route('catalog.products')->with('successMessage', 'Products Sync Successfully');

        } else if ($response->result == 'error') {

            return redirect()->route('catalog.products')->with('errorMessage', $response->messages[0]);

        }
    }

    public function createInnerProduct($optionlists, $itemNo, $sunKey, $route)
    {
        foreach ($optionlists->items as $optionlist) {
            $optionListResponse = $sunKey->call($route, array('itemNo' => $optionlist->itemNo));
            if ($optionListResponse->result == 'success') {
                $optionListProduct = $optionListResponse->data;
                $pl_description = isset($optionListProduct->description) ? $optionListProduct->description : "";
                $pl_paramsTable = isset($optionListProduct->paramsTable) ? $optionListProduct->paramsTable : "";
                $pl_description = $pl_description . $pl_paramsTable;

                $innerModel = Product::updateOrCreate(['itemNo' => $optionListProduct->itemNo],
                    [
                        'title' => isset($optionListProduct->name) ? $optionListProduct->name : '',
                        'body_html' => $pl_description,
                        'price' => isset($optionListProduct->price) ? $optionListProduct->price : 0
                    ]
                );
                /* Inner varient*/
                if (isset($optionListProduct->modelList)) {
                    $this->addVarient($optionListProduct->modelList, $itemNo, $sunKey, $route, $innerModel);
                }
            }
        }
    }

    public function sync(Request $request, $id)
    {
        $model = Product::find($id);
        //check if the product has images

        $pHasImage = DB::table('downimagestatus')->where([
            ['product_id', $id],
            ['status', true]
        ])->exists();
        if ($pHasImage == '') {
            return redirect()->route('catalog.products')->with('errorMessage', 'please first download images of  '.$model->itemNo);
        }else{
            $this->callSync($model);
            return redirect()->route('catalog.products')->with('successMessage', 'Products Sync Successfully');
        }
    }

    public function addVarient($modelLists, $itemNo, $sunKey, $route, $modal)
    {
        foreach ($modelLists as $modelList) {
            $modelListItemNo = $modelList->key;
            $modelListResponse = $sunKey->call($route, array('itemNo' => $modelListItemNo));
            if ($modelListResponse->result == 'success') {
                $modelListProduct = $modelListResponse->data;
                $title = isset($modelListProduct->name) ? $modelListProduct->name : '';
                $title = $this->getBetween($title, '(', ')');
                $vModal = Variant::updateOrCreate([
                    'sku' => $modelListProduct->itemNo
                ], [
                    'product_id' => $modal->id,
                    'price' => isset($modelListProduct->price) ? $modelListProduct->price : '',
                    'barcode' => isset($modelListProduct->barcode) ? $modelListProduct->barcode : '',
                    'title' => $title,
                    'inventory_quantity' => 2001,
                    'grams' => isset($modelListProduct->unitWeight) ? $modelListProduct->unitWeight : ''
                ]);
            }
        }
    }


    // Edit product

    public function edit(Request $request, $id)
    {
        $model = Product::find($id);
        $pageTitle = __('system.edit') . " " . $model->itemNo;
        $breadcrumbs = [['text' => __('system.edit')]];
        $viewParams = [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle
        ];
        return view('products.edit', $viewParams);
    }

    // Update Product

    public function update(Request $request, $id)
    {
        $model = Product::find($id);
        $model->price = $request->input('price', null);
        $model->vendor = $request->input('vendor', null);
        $model->title = $request->input('title', null);
        $model->body_html = $request->input('body_html', null);
        $model->is_updated = true;
        if ($model->save()) {
            /* Ready to update the variants*/
            $variants = $request->input('variants', null);
            $length = count($variants['title']);
            for ($i = 0; $i < $length; $i++) {
                Variant::where('id', '=', $variants['id'][$i])
                    ->update([
                        'sku' => $variants['sku'][$i],
                        'title' => $variants['title'][$i],
                        'price' => $variants['price'][$i],
                        'inventory_quantity' => $variants['inventory_quantity'][$i],
                        'barcode' => $variants['barcode'][$i],
                        'grams' => $variants['grams'][$i],
                        'is_updated' => true
                    ]);

            }
            return redirect()->route('catalog.products')->with('successMessage', $model->itemNo . " " . __('system.updated_success_message'));
        } else {
            return redirect()->back()
                ->withErrors($model->itemNo . " " . __('system.updated_failed_message_message'))
                ->withInput();
        }
    }


    public function multisync(Request $request)
    {
        $ids = $request->id;
        if ($ids == '') {
            return redirect()->back()
                ->withErrors('Please Select a Product')
                ->withInput();
        } else {
            foreach ($ids as $id) {
                $model = Product::find($id);
                $this->callSync($model);
            }
            return redirect()->route('catalog.products')->with('successMessage', 'Products Sync Successfully');
        }
    }

    public function callSync($model)
    {
        $product = [
            "id" => $model->itemNo,
            "body_html" => $model->body_html,
            "title" => $model->title,
            "vendor" => $model->vendor,
            "published_scope" => "global",
            "price" => $model->price,
            "options" => [['name' => 'Color']]
        ];
        // Ready to create product in shopify
        $shopify = new Shopify();

        if ($model->is_created) {

            $response = $shopify->updatedProduct($model->product_id, $product);
            $model->is_updated = true;
            $model->save();


        } else {

            $variants_array = array();
            if ($model->variants->count() > 0) {
                foreach ($model->variants->toArray() as $variant) {
                    $variants_array[] = array(
                        'option1' => $variant['title'],
                        'price' => trim($variant['price']),
                        'sku' => trim($variant['sku']),
                        'barcode' => $variant['barcode'],
                        'grams' => $variant['grams'],
                        'inventory_quantity' => $variant['inventory_quantity'],
                        'inventory_management' => 'shopify',
                        'product_id' => $model->product_id,
                    );
                }
            }
            $product['variants'] = $variants_array;
            $response = $shopify->createProduct($product);
            if (count($response["product"]["variants"]) > 0) {
                foreach ($response["product"]["variants"] as $rvariant) {
                    Variant::where('sku', $rvariant['sku'])->update(['shopify_id' => $rvariant['id']]);
                }
            }
            $model->shopify_id = $response['product']['id'];
            $model->is_created = true;
            $model->save();

            $this->syncVariantImages($model);
            $this->syncProductImages($model);

        }
    }

    public function syncProductImages($model)
    {
        $directory = public_path() . "/upload/products/" . $model->itemNo . "/";
        if (file_exists($directory)) {
            $handler = opendir($directory);
            while ($file = readdir($handler)) {
                if ($file != "." && $file != "..") {
                    $parameters = [
                        'image' => [
                            'src' => asset('upload/products/' . $model->itemNo . "/" . $file)
                        ]
                    ];
                    $shopify = new Shopify();
                    $shopify->addProductImages($model->shopify_id, $parameters);
                }
            }
        }
    }


    public function syncVariantImages($model)
    {
        $variants = Variant::where('product_id', $model->id)->get();
        foreach ($variants as $variant) {
            $directory = public_path() . "/upload/products/" . $variant->sku . "/";
            if (file_exists($directory)) {
                $handler = opendir($directory);
                $counter = 0;
                while ($file = readdir($handler)) {
                    if ($file != "." && $file != "..") {
                        if ($counter == 2) {
                            $vImageData = [
                                'image' => [
                                    "variant_ids" => [$variant->shopify_id],
                                    "filename" => asset('upload/products/' . $model->itemNo . "/" . $file)
                                ]
                            ];
                            $shopify = new Shopify();
                            $shopify->updatedProductVariantImage($model->shopify_id, $vImageData);
                        }
                    }
                    $counter++;
                }
            }
        }
    }

    public function imageDownload($itemNo)
    {
       $sunkey = new SunKey();
        $for = "product!getImages.do";
        $parameters = array('itemNo' => $itemNo);
        $path = public_path() . "/upload/products/" . $parameters['itemNo'] . '.zip';
        $sunkey->downloadProductImages($for, $path, $parameters);
        $isValid = Zip::check($path);
        if ($isValid == true) {
            $zip = Zip::open($path);
            $zip->extract(public_path() . "/upload/products/" . $parameters['itemNo']);
            $zip->close();
            $product_id = Product::where('itemNo', $itemNo)->value('id');
            $variants = Variant::where('product_id', $product_id)->get();
            foreach ($variants as $variant) {
                $vPath = public_path() . "/upload/products/" . $variant->sku . '.zip';
                $sunkey->downloadProductImages($for, $vPath, array('itemNo' => $variant->sku));
                $isValid = Zip::check($vPath);
                if ($isValid == true) {
                    $vZip = Zip::open($vPath);
                    $vZip->extract(public_path() . "/upload/products/" . $variant->sku);
                    $vZip->close();
                }
            }
            DB::table('downimagestatus')->insert(array('product_id' => $product_id, 'status' => true));
            /*delete zip files*/
            foreach (glob(public_path() . "/upload/products/"."*.zip") as $filename) {
                unlink($filename);
            }
            return redirect()->route('catalog.product.images', [$itemNo])->with('successMessage', 'Images of the ' . $parameters['itemNo'] . ' Downloaded Successfully');
        }
    }

    public function images($itemNo)
    {
        $results = array();
        $directory = public_path() . "/upload/products/" . $itemNo . "/";
        if (file_exists($directory)) {
            $handler = opendir($directory);
            while ($file = readdir($handler)) {
                if ($file != "." && $file != "..") {
                    $results[] = asset('upload/products/' . $itemNo . "/" . $file);
                }
            }
        }
        $model = Product::where('itemNo', $itemNo)->first();
        $variants = $model->variants()->get();
        $variantResults = array();
        if (count($variants) > 0) {
            foreach ($variants as $variant) {
                $vdirectory = public_path() . "/upload/products/" . $variant->sku . "/";
                if (file_exists($vdirectory)) {
                    $vdirectory = opendir($vdirectory);
                    $counter = 0;
                    while ($vfile = readdir($vdirectory)) {
                        if ($vfile != "." && $vfile != "..") {
                            if ($counter == 2) {
                                $variantResults[$variant->title] = asset('upload/products/' . $variant->sku . "/" . $vfile);
                            }
                        }
                        $counter++;
                    }
                }
            }
        }
        $model->variantImages = $variantResults;
        $model->images = $results;
        $pageTitle = __('system.all_images') . " " . $model->itemNo;
        $breadcrumbs = [['text' => __('system.all_images')]];
        $viewParams = [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle
        ];
        return view('products.images', $viewParams);
    }
}
