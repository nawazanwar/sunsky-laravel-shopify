<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Setting extends Model implements Permissions
{
    protected $fillable = ['name', 'value', 'type'];

    public function getMaintenance()
    {
        return DB::table('settings')->where('name','maintenance')->value('value');
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_setting');
                    break;
                case 'create':
                case 'store':
                    return array('create_setting');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_setting');
                    break;
                case 'delete':
                    return array('delete_setting');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_setting',
            'create_setting',
            'edit_setting',
            'delete_setting',
        );
    }
}
