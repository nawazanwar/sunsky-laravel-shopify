<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable implements Permissions
{

    use SoftDeletes;
    use Notifiable;
    protected $fillable = [
        'name', 'email', 'password', 'active', 'temp_password', 'image', 'cover'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $table = 'users';
    protected $dates = ['deleted_at'];

    public function getImage($size = 'full')
    {
        foreach ($this->media as $m) {
            if ($m->size == $size) {
                return asset('uploads/users/' . $m->path);
            }
        }
    }

    public function getImageAttribute($value)
    {
        return $value ? '/uploads/users/' . $value : '';
    }

    public function getCoverAttribute($value)
    {
        return $value ? '/uploads/users/' . $value : '';
    }

    public function getAvatar($size = '70')
    {
        $images = $this->media->pluck('path', 'size')->toArray();
        return !empty($images) ? '/uploads/users/' . $images[$size] : null;
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
    public function grades()
    {
        return $this->belongsToMany(Grade::class);
    }
    public function media()
    {
        return $this->belongsToMany(Media::class);
    }

    public function hasAlreadyRole($user_id, $role_id)
    {
        return DB::table('role_user')->where(['role_id' => $role_id, 'user_id' => $user_id])->exists();
    }

    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return !!$role->intersect($this->roles);

    }

    public function ability($permission = null)
    {
        return !is_null($permission) && $this->checkPermission($permission);
    }


    protected function checkPermission($permission)
    {
        $permissions = $this->getAllPermissionsFromAllRoles();

        if ($permissions === true) {
            return true;
        }

        $permissionArray = is_array($permission) ? $permission : [$permission];
        return count(array_intersect($permissions, $permissionArray));
    }

    protected function getAllPermissionsFromAllRoles()
    {
        $permissions = array();
        $roles = $this->roles->load('permissions');

        if (!$roles) {
            return true;
        }

        foreach ($roles as $role) {
            foreach ($role->permissions as $permission) {
                $permissions[] = $permission->toArray();
            }
        }

        return array_map('strtolower', array_unique($this->array_flatten(array_map(function ($permission) {
            return $permission['name'];
        }, $permissions))));

    }

    function array_flatten($array)
    {
        if (!is_array($array)) {
            return FALSE;
        }
        $result = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, array_flatten($value));
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_user');
                    break;
                case 'create':
                case 'store':
                    return array('create_user');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_user');
                    break;
                case 'delete':
                    return array('delete_user');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_user',
            'create_user',
            'edit_user',
            'delete_user',
        );
    }
}
