<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model implements Permissions
{

    protected $fillable = ['itemNo', 'title', 'body_html','shopify_id','price'];

    public function variants()
    {
        return $this->hasMany(Variant::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_product');
                    break;
                case 'create':
                case 'store':
                    return array('create_product');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_product');
                    break;
                case 'delete':
                    return array('delete_product');
                    break;
                case 'sync':
                    return array('sync_product');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_product',
            'create_product',
            'edit_product',
            'delete_product',
            'sync_product'
        );
    }
}
