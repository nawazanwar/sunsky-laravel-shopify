<?php

namespace App\Models;

use App\Traits\Configuration;
use Illuminate\Database\Eloquent\Model;

class SunKey extends Model
{
    use Configuration;

    public function call($for, $parameters)
    {
        $parameters['key'] = $this->getSunKey();
        $signature = $this->sign($parameters);
        $parameters['signature'] = $signature;
        $postdata = http_build_query($parameters);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->getSunApiUrl() . $for);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        $output = curl_exec($ch);
        curl_close($ch);
        return json_decode($output);
    }

    public function downloadProductImages($for,$path,$parameters)
    {
        $parameters['key'] = $this->getSunKey();
        $signature = $this->sign($parameters);
        $parameters['signature'] = $signature;
        $postdata                = http_build_query($parameters);
        $fp                      = fopen($path, 'w');
        $ch                      = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $this->getSunApiUrl() . $for );
        curl_setopt( $ch, CURLOPT_FILE, $fp );
        curl_setopt( $ch, CURLOPT_HEADER, 0 );
        curl_setopt( $ch, CURLOPT_POST, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $postdata );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 100000 );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 100000 );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_exec( $ch );
        curl_close( $ch );
        fclose( $fp );
    }

    public function sign($parameters)
    {
        $signature = '';
        ksort($parameters);
        foreach ($parameters as $key => $value) {
            $signature .= $value;
        }
        $signature = $signature . '@' . $this->getSunSecret();
        $signature = md5($signature);
        return $signature;
    }

}
