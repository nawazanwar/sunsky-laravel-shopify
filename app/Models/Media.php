<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = ['title', 'url', 'path', 'type', 'size'];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
