<?php

namespace App\Models;

use App\Traits\Configuration;
use Illuminate\Database\Eloquent\Model;

class Shopify extends Model
{
    use Configuration;

    public function addProductImages($product_id, $parameters = array())
    {
        $url = $this->getShopifyUrl() . "products/" . $product_id . "/images.json";
        return $this->call($url, 'POST', $parameters);
    }

    public function updatedProductVariantImage($product_id, $parameters = array())
    {
        $url = $this->getShopifyUrl() . "products/" . $product_id . "/images.json";
        return $this->call($url, 'POST', $parameters);
    }


    public function updatedProduct($product_id, $parameters = array())
    {
        $parameters = array("product" => $parameters);
        return $this->call($this->getShopifyUrl() . "products/" . $product_id . ".json", 'PUT', $parameters);
    }

    public function createProduct($parameters = array())
    {
        $parameters = array("product" => $parameters);
        return $this->call($this->getShopifyUrl() . "products.json", 'POST', $parameters);
    }

    public function getProducts()
    {
        return $this->call($this->getShopifyUrl() . "products.json");
    }

    public function syncProductVarients($product_id, $varients)
    {

        foreach ($varients as $key => $value) {
            $parameters = array("variant" => array(
                'option1' => $value['title'],
                'price' => trim($value['price']),
                'SKU' => trim($value['sku']),
                'barcode' => $value['barcode']
            ));
            $url = $this->getShopifyUrl() . "products/{$product_id}/variants.json";
            $this->call($url, 'POST', $parameters);
        }
        //$product["variants"] = $varients;
    }

    public function call($url, $type = 'GET', $data = null)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);

        if ($type == 'PUT' || $type == 'POST')
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        if ($type != 'GET')
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);

        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }
}
