<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Brand extends Model implements Permissions
{

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_brand');
                    break;
                case 'create':
                case 'store':
                    return array('create_brand');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_brand');
                    break;
                case 'delete':
                    return array('delete_brand');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_brand',
            'create_brand',
            'edit_brand',
            'delete_brand',
        );
    }
}
