<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Variant extends Model implements Permissions
{

    protected $fillable = ['sku', 'title', 'barcode', 'inventory_quantity', 'grams', 'price', 'product_id', 'shopify_id'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_variant');
                    break;
                case 'create':
                case 'store':
                    return array('create_variant');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_variant');
                    break;
                case 'delete':
                    return array('delete_variant');
                    break;
                case 'sync':
                    return array('sync_variant');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_variant',
            'create_variant',
            'edit_variant',
            'delete_variant',
            'sync_variant'
        );
    }

}
