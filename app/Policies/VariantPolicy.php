<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class VariantPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_variant');
    }

    public function create(User $user)
    {
        return $user->ability('create_variant');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_variant');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_variant');
    }

    public function sync(User $user)
    {
        return $user->ability('sync_variant');
    }

}
