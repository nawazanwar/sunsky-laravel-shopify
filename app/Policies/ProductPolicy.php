<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class ProductPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_product');
    }

    public function create(User $user)
    {
        return $user->ability('create_product');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_product');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_product');
    }

    public function sync(User $user)
    {
        return $user->ability('sync_product');
    }

}
